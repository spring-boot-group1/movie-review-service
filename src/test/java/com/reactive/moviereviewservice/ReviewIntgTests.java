package com.reactive.moviereviewservice;

import com.reactive.moviereviewservice.domain.Review;
import com.reactive.moviereviewservice.repository.ReviewRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ImportAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
public class ReviewIntgTests {

    @Autowired
    WebTestClient webTestClient;

    @Autowired
    ReviewRepository reviewRepository;

    static String REVIEW_URL = "/v1/reviews";

    @BeforeEach
    void setUp() {

        var reviews = List.of(
                new Review(null,1L,"Excellect Movie",9.0),
                new Review(null,1L,"Good Movie",8.0),
                new Review(null,2L,"Average Movie",7.5)
        );
        reviewRepository.saveAll(reviews).blockLast();
    }

    @AfterEach
    void tearDown() {
        reviewRepository.deleteAll().block();
    }

    @Test
    void addMovieReview() {

        var review = new Review(null,1L,"Worst Movie",1.0);

        webTestClient
                .post()
                .uri(REVIEW_URL)
                .bodyValue(review)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(Review.class)
                .consumeWith(reviewEntityExchangeResult -> {
                    var savereview = reviewEntityExchangeResult.getResponseBody();
                    assert savereview != null;
                    assert savereview.getReviewId() != null;
                });
    }

    @Test
    void getMovieReview() {

        webTestClient
                .get()
                .uri(REVIEW_URL)
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBodyList(Review.class)
                .hasSize(3);
    }

}
