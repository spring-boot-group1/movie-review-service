package com.reactive.moviereviewservice;

import com.reactive.moviereviewservice.domain.Review;
import com.reactive.moviereviewservice.handler.ReviewHandler;
import com.reactive.moviereviewservice.repository.ReviewRepository;
import com.reactive.moviereviewservice.router.ReviewRouter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@WebFluxTest
@ContextConfiguration(classes = {ReviewHandler.class, ReviewRouter.class})
@AutoConfigureWebTestClient
public class ReviewsUnitTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ReviewRepository reviewRepository;

    @Test
    void addReviews() {

        var review = new Review(null,1L,"Worst Movie",1.0);

        when(reviewRepository.save(isA(Review.class)))
                .thenReturn(Mono.just(new Review("abc",1L,"Worst Movie",1.0)));

        webTestClient
                .post()
                .uri("/v1/reviews")
                .bodyValue(review)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(Review.class)
                .consumeWith(movieInfoEntityExchangeResult -> {
                    var saveMovieReview = movieInfoEntityExchangeResult.getResponseBody();
                    assert saveMovieReview != null;
                    assert saveMovieReview.getReviewId() != null;
                    assertEquals("abc",saveMovieReview.getReviewId());
                });
    }

}
