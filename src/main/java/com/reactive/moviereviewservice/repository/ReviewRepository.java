package com.reactive.moviereviewservice.repository;

import com.reactive.moviereviewservice.domain.Review;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface ReviewRepository extends ReactiveMongoRepository<Review,String> {
    Flux<Review> findByMovieInfoId(Long movieId);
}
