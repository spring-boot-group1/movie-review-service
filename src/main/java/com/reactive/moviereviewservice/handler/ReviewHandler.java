package com.reactive.moviereviewservice.handler;

import com.reactive.moviereviewservice.domain.Review;
import com.reactive.moviereviewservice.repository.ReviewRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ReviewHandler {

    private ReviewRepository reviewRepository;

    public ReviewHandler(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public Mono<ServerResponse> addReview(ServerRequest request) {
        return request.bodyToMono(Review.class)
                .flatMap(review -> {
                    return reviewRepository.save(review);
                })
                .flatMap(savedReview -> {
                    return ServerResponse.status(HttpStatus.CREATED).bodyValue(savedReview);
                });
    }

    public Mono<ServerResponse> getReviews(ServerRequest request) {
        var movieId = request.queryParam("movieInfoId");

        if (movieId != null){
            var reviewFlux = reviewRepository.findByMovieInfoId(Long.valueOf(movieId.get()));
            return ServerResponse.ok().body(reviewFlux, Review.class);
        }
        else{
            var reviewFlux = reviewRepository.findAll();
            return ServerResponse.ok().body(reviewFlux, Review.class);
        }
    }

    public Mono<ServerResponse> updateReview(ServerRequest request) {
        var reviewId = request.pathVariable("id");
        var existingReview = reviewRepository.findById(reviewId);

        return existingReview
                .flatMap(review -> request.bodyToMono(Review.class)
                        .map(reqReview -> {
                            review.setComment(reqReview.getComment());
                            review.setRating(reqReview.getRating());
                            return review;
                        })
                        .flatMap(updatedReview -> reviewRepository.save(updatedReview))
                        .flatMap(savedReview -> ServerResponse.ok().bodyValue(savedReview))
                );
    }

    public Mono<ServerResponse> deleteReview(ServerRequest request) {
        var reviewId = request.pathVariable("id");
        var deletedId = reviewRepository.deleteById(reviewId);
        return deletedId.then(ServerResponse.noContent().build());
    }
}
